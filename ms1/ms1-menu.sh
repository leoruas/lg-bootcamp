#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS:
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
while [[ true ]]; do
  echo -e "Options:\n(1) Shutdown.\n(2) Restart.\n(3) Turn Screen.\n(4) Change Keyboard Language.\n(5) Quit.\n"
  # -e -> enable interpretation of backslash escapes
  read -p "Choose an operation: " op;

  case $op in
    1)
      bash ./shutdown.sh
      break;
      ;;
    2)
      bash ./restart.sh
      break;
      ;;
    3)
      read -p "Choose the orientation: " or
      bash ./turn-screen.sh $or
      break;
      ;;
    4)
      read -p "Choose the language code: " code
      bash ./keyboard-language.sh $code
      break;
      ;;
    5)
      echo "Quitting menu..."
      break;
      ;;
    *)
      echo "Invalid Operation"
      ;;
  esac
done
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
