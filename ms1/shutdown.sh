#===============================================================================
#USAGE : ./shutdown.sh
#DESCRIPTION : Power off your machine using an script.
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
while true; do
  read -p "Are you sure you want to shut down your computer? (Y/n): " confirm;

  if [ ${confirm,} = 'y' ] || [ ${confirm,} = 'n' ]; then
    break;
  fi
  #keep asking for confirmation until confirm is either n or y
done;

if [ ${confirm,} = 'y' ]; then
  # Compares lower case confirm
  shutdown -h now;
  echo "Shutting down...";
else
  echo "Shut down canceled.";
fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
